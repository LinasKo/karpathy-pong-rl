from slacker import Slacker


class SlackMessenger(object):
    def __init__(self, slack_token, slack_channel):
        self.slack = Slacker(slack_token)
        self.channel = slack_channel

    def post(self, message):
        res = self.slack.chat.post_message(self.channel, message, as_user=True)        
        self.prev_channel_id = res.body['channel']
        self.prev_msg_timestamp = res.body['ts']
        return self.prev_msg_timestamp, self.prev_channel_id

    def update(self, message, channel_id=None, msg_timestamp=None):
        if channel_id == None or msg_timestamp == None:
            channel_id = self.prev_channel_id
            msg_timestamp = self.prev_msg_timestamp
        self.slack.chat.update(channel_id, msg_timestamp, message)

    def upload_file(self, file_path):
        self.slack.files.upload(file_path, channels=[self.channel])        

    @staticmethod
    def make_bar(percent_done):
        rounded_percent = round(percent_done)
        return "`<%s%s> %s%%`" % ('=' * (rounded_percent // 2), ' ' * ((100 - rounded_percent) // 2), rounded_percent)
